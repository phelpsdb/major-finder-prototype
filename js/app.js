'use strict';
angular.module('majorFinder', ['ngAnimate']);

(function(app) {
	app.directive('mfSearchbar', function() {
		return {
			restrict: 'E',
			template: [
				'<div>',
				'	<input type="text" ng-model="mfSearchQuery">',
				'	<span class="glyphicon glyphicon-search"></span>',
				'</div>'
			].join(''),
			link: function(scope, elem, attrs) {
				elem.find('input').attr('placeholder', attrs.hint);
			}
		};
	});
	app.directive('mfHeader', function() {
		return {
			restrict: 'E',
			scope: {
				title: '@',
				leftButtonClick: '&',
				leftButtonIcon: '@',
				rightButtonClick: '&',
				rightButtonIcon: '@'
			},
			template: [
				'<div>',
				'	<button class="left-button" ng-click="leftButtonClick()">',
				'		<span class="glyphicon" ng-class="leftButtonIcon"></span>',
				'		{{leftButtonText}}',
				'	</button>',
				'	<h1>{{title}}</h1>',
				'	<button class="right-button" ng-click="rightButtonClick()">',
				'		<span class="glyphicon" ng-class="rightButtonIcon"></span>',
				'		{{rightButtonText}}',
				'	</button>',
				'</div>'
			].join('')
		};
	});

	app.controller('majorFinder', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
		$scope.majors = [];
		$scope.allSchools = [];
		$scope.selectedSchools = [];

		$scope.filtersOpen = false;
		$scope.filters = [
			{
				name: "New Grad Income",
				options: [
					{ val:200000, label:"> $200,000" },
					{ val:150000, label:"> $150,000" },
					{ val:100000, label:"> $100,000" },
					{ val:75000, label:"> $75,000" },
					{ val:50000, label:"> $50,000" },
					{ val:25000, label:"> $25,000" }
				]
			}, {
				name: "Average Income",
				options: [
					{ val:200000, label:"> $200,000" },
					{ val:150000, label:"> $150,000" },
					{ val:100000, label:"> $100,000" },
					{ val:75000, label:"> $75,000" },
					{ val:50000, label:"> $50,000" },
					{ val:25000, label:"> $25,000" }
				]
			}, {
				name: "Five Year Outlook",
				options: [
					{val:'good', label:'Good'},
					{val:'so-so', label:'So-so'},
					{val:'bad', label:'Bad'}
				]
			}, {
				name: "Unemployment",
				options: [
					{val:0.5, label:'> 50%'},
					{val:0.25, label:'> 25%'},
					{val:0.1, label:'> 10%'},
					{val:0.05, label:'> 5%'},
					{val:0.025, label:'> 2.5%'}
				]
			}, {
				name: "Major Easiness",
				options: [
					{val:5, label: '5 stars'},
					{val:4, label: '> 4 stars'},
					{val:3, label: '> 3 stars'},
					{val:2, label: '> 2 stars'},
					{val:1, label: '> 1 star'}
				]
			}, {
				name: "Overall Rating",
				options: [
					{val:5, label: '5 stars'},
					{val:4, label: '> 4 stars'},
					{val:3, label: '> 3 stars'},
					{val:2, label: '> 2 stars'},
					{val:1, label: '> 1 star'}
				]
			}, {
				name: "Job Satisfaction",
				options: [
					{val:5, label: '5 stars'},
					{val:4, label: '> 4 stars'},
					{val:3, label: '> 3 stars'},
					{val:2, label: '> 2 stars'},
					{val:1, label: '> 1 star'}
				]
			}
		];

		$scope.filterMajor = function(val) {
			return (!$scope.filters[1].selectedValue || val.avgIncome > $scope.filters[1].selectedValue) &&
				(!$scope.filters[0].selectedValue || val.avgGradIncome > $scope.filters[0].selectedValue) &&
				(!$scope.filters[2].selectedValue || val.fiveYearOutlook === $scope.filters[2].selectedValue) &&
				(!$scope.filters[3].selectedValue || val.unemploymentRate > $scope.filters[3].selectedValue) &&
				(!$scope.filters[4].selectedValue || val.easiness > $scope.filters[4].selectedValue) &&
				(!$scope.filters[5].selectedValue || val.overallRating > $scope.filters[5].selectedValue) &&
				(!$scope.filters[6].selectedValue || val.jobSatisfaction > $scope.filters[6].selectedValue) &&
				(!$scope.filterFavorites || val.favorite);
		};

		$scope.filterFavorites = false;

		$scope.toggleFilterFavorites = function() {
			$scope.filterFavorites = !$scope.filterFavorites;
		};

		$scope.clearFilter = function(filter) {
			filter.selectedValue = null;
		};

		$scope.isFavorite = function(major) { 
			return (major.favorite && 'glyphicon-star') || 'glyphicon-star-empty';
		};

		$scope.expandMajor = function(major, e) {
			if ($scope.activeMajor === major)
				$scope.activeMajor = null;
			else
				$scope.activeMajor = major
		};

		$scope.toggleFavorite = function (major, e) {
			e.stopImmediatePropagation();
			major.favorite = !major.favorite;
		};

		$scope.goHome = function() {
			$rootScope.activeView = "homePage";
		};

		$scope.goToSchools = function() {
			$rootScope.activeView = "schoolSelector";
		};

		$scope.toggleFilters = function() {
			$scope.filtersOpen = !$scope.filtersOpen;
		};

		$http.get('js/degrees.json').success(function (data) {
			$scope.allSchools = data.schools;
			$scope.selectedSchools = $scope.allSchools;
			$scope.majors = [];
			$scope.selectedSchools.forEach(function(school) {
				school.majors.forEach(function(major) { major.school = school.name; });
				$scope.majors = $scope.majors.concat(school.majors);
			});
		}).error(function(err) {
			console.log('Error loading major data: ' + err);
		});
	}]);

	app.controller('schoolSelector', ['$rootScope', '$scope', function($rootScope, $scope) {
		$scope.schools = [
			{ name: 'Brigham Young University' },
			{ name: 'Brigham Old University' },
			{ name: 'Young Brigham University' },
			{ name: 'Another University' },
			{ name: 'Stuff University' },
			{ name: 'Men\'s University' },
			{ name: 'Womens\'s University' },
			{ name: 'French University' },
			{ name: 'University University' },
			{ name: 'Backstabbing University' },
			{ name: 'Bacon and cheese University' },
			{ name: 'Ugh #thisSucks University' },
			{ name: 'Bob Jenkins University' },
			{ name: 'Hanna Montana University' },
			{ name: 'There\'s a University' },
			{ name: 'That\'s a University' },
			{ name: 'Whatever University' },
			{ name: 'Huge University' },
			{ name: 'Young University' }
		];

		$scope.toggleSelectSchool = function(school) {
			school.selected = !school.selected;
		};

		$scope.backToMajors = function() {
			$rootScope.activeView = 'majorFinder';
		};
	}]);

	app.controller('homePage', ['$rootScope', '$scope', function($rootScope, $scope) {
		$scope.showNotSupported = false;

		$scope.goToFindMajors = function() {
			$rootScope.activeView = "majorFinder";
		};

		$scope.notSupported = function() {
			$scope.showNotSupported = true;
		};

		$scope.hideNotSupported = function() {
			$scope.showNotSupported = false;
		};

		$scope.submitOnEnter = function(e) {
			if (e.keyCode === 13) { // enter pressed
				$rootScope.activeView = 'majorFinder';
			}
		};

	}]);

})(angular.module('majorFinder'));


